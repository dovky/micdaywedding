/* ------------------------------------------------------------------------------
	
  Name: Michael Deyna
	Version: 1.0
  Description: Michael & Deyna Wedding
  License: ISC
  
  ------------------------------------------------------------------------------ */
//

// Navbar
var setNavbar = document.querySelector("[data-navbar]");

// Target
let getTarget = document.querySelectorAll("[data-toggle]");

if (getTarget.length !== 0) {
  getTarget.forEach((tgetEl) => {
    var getAttrTarget = tgetEl.getAttribute("data-target");
    var getDismiss = tgetEl.getAttribute("data-dismiss");

    if (getAttrTarget !== "") {
      tgetEl.addEventListener("click", (e) => {
        var setAttrTarget = document.querySelector(getAttrTarget);

        if (setAttrTarget !== null) {
          setAttrTarget.classList.contains("active") !== true
            ? setAttrTarget.classList.add("active")
            : setAttrTarget.classList.remove("active");
        }

        e.preventDefault();
      });
    }

    if (getDismiss !== "") {
      tgetEl.addEventListener("click", (e) => {
        var setDismiss = document.querySelector(getDismiss);

        if (setDismiss !== null) {
          setDismiss.classList.contains("active") !== true
            ? setDismiss.classList.add("active")
            : setDismiss.classList.remove("active");
        }

        e.preventDefault();
      });
    }
  });
}

// Data Countdown
var getTgetCountdown = document.querySelector("[data-target-coutdown]");

if (getTgetCountdown !== null) {
  let getDays = document.querySelector("[data-days]");
  let getHours = document.querySelector("[data-hours]");
  let getMinutes = document.querySelector("[data-minutes]");
  let getSeconds = document.querySelector("[data-seconds]");

  var getAttrTgetCountdown = getTgetCountdown.getAttribute(
    "data-target-coutdown"
  );

  if (getAttrTgetCountdown !== "") {
    var setDates = new Date(getAttrTgetCountdown);
    var setCurrentYear = new Date().getFullYear();

    setInterval(() => {
      var setCurrentTime = new Date();
      const setDistance = setDates - setCurrentTime;

      if (getDays !== null) {
        const setDistanceDay = Math.floor(setDistance / (1000 * 60 * 60 * 24));

        getDays.innerHTML = setDistanceDay;
      }
      if (getHours !== null) {
        const setDistanceHours = Math.floor(
          (setDistance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
        );

        getHours.innerHTML = setDistanceHours;
      }
      if (getMinutes !== null) {
        const setDistanceMinutes = Math.floor(
          (setDistance % (1000 * 60 * 60)) / (1000 * 60)
        );

        getMinutes.innerHTML = setDistanceMinutes;
      }
      if (getSeconds !== null) {
        const setDistanceSeconds = Math.floor(
          (setDistance % (1000 * 60)) / 1000
        );

        getSeconds.innerHTML = setDistanceSeconds;
      }

      if (setDistance < 0) {
        clearInterval();
        if (getDays !== null) {
          getDays.innerHTML = "00";
        }
        if (getHours !== null) {
          getHours.innerHTML = "00";
        }
        if (getMinutes !== null) {
          getMinutes.innerHTML = "00";
        }
        if (getSeconds !== null) {
          getSeconds.innerHTML = "00";
        }
      }
    }, 1000);
  }
}

// Data Dissimis
let getDismis = document.querySelectorAll("[data-dissimis]");
let getDismis_Transtion = document.querySelectorAll(
  "[data-dissimis-transition]"
);

if (getDismis.length !== 0) {
  getDismis.forEach((tgetEl) => {
    tgetEl.addEventListener("click", (e) => {
      var getLanding = tgetEl.getAttribute("data-landing") !== null;

      if (getLanding == true) {
        var getAttr_Dismis = tgetEl.getAttribute("data-dissimis");

        if (getAttr_Dismis !== "") {
          var setEl_Dismis = document.querySelector(getAttr_Dismis);

          if (setEl_Dismis !== null) {
            if (!setEl_Dismis.classList.contains("hidden")) {
              setEl_Dismis.classList.add("hidden");
            } else {
              setEl_Dismis.classList.remove("hidden");
            }
          }
        }
      }

      e.preventDefault();
    });
  });
}

if (getDismis_Transtion.length !== 0) {
  getDismis_Transtion.forEach((tgetEl) => {
    tgetEl.addEventListener("click", (e) => {
      var getAttr_Dismis = tgetEl.getAttribute("data-dissimis-transition");

      if (getAttr_Dismis !== "") {
        var setEl_Dismis = document.querySelector(getAttr_Dismis);

        if (setEl_Dismis !== "") {
          setTimeout(() => {
            if (!setEl_Dismis.classList.contains("hidden")) {
              setEl_Dismis.classList.add("hidden");
            } else {
              setEl_Dismis.classList.remove("hidden");
            }
          }, 400);
        }
      }

      e.preventDefault();
    });
  });
}

// Scroll
let getScroll = document.querySelectorAll("[data-scroll]");

if (getScroll.length !== 0) {
  getScroll.forEach((tgetEl) => {
    var setAttrScroll = tgetEl.getAttribute("data-scroll");

    if (setAttrScroll !== "") {
      if (setAttrScroll === "up") {
        tgetEl.addEventListener("click", (e) => {
          window.scroll({
            top: 0,
            left: 0,
            behavior: "smooth",
          });

          e.preventDefault();
        });
      } else if (setAttrScroll === "down") {
        tgetEl.addEventListener("click", (e) => {
          window.scrollTo({
            top: document.body.scrollHeight,
            left: 0,
            behavior: "smooth",
          });

          e.preventDefault();
        });
      }
    }
  });
}

// Scroll To
var getScrollTo = document.querySelectorAll("[data-scrollto]");

if (getScrollTo.length !== 0) {
  getScrollTo.forEach((tgetEl) => {
    tgetEl.addEventListener("click", (e) => {
      var getAttr_Href = tgetEl.getAttribute("href");
      var getNavMenu = document.querySelectorAll("[data-menu] [data-items]");

      // if (getNavMenu.length !== 0) {
      //   getNavMenu.forEach((tgetMenuEl) => {
      //     tgetMenuEl.classList.remove("active");
      //   });
      // }

      if (getAttr_Href !== "") {
        var setElmnTarget = document.querySelector(getAttr_Href);

        if (setElmnTarget !== null) {
          var checkOffsetTop;

          if (!tgetEl.parentElement.classList.contains("active")) {
            tgetEl.parentElement.classList.add("active");
          }

          setTimeout(() => {
            var getDismiss = tgetEl.getAttribute("data-dismiss");

            if (getDismiss !== "") {
              var setDismiss = document.querySelector(getDismiss);

              if (setDismiss !== null) {
                if (setDismiss.classList.contains("active")) {
                  setDismiss.classList.remove("active");
                }
              }
            }
          }, 600);

          if (setElmnTarget.offsetTop > 0) {
            checkOffsetTop = setElmnTarget.offsetTop - setNavbar.clientHeight;
          } else {
            checkOffsetTop = setElmnTarget.offsetTop;
          }

          window.scrollTo({
            top: checkOffsetTop,
            left: 0,
            behavior: "smooth",
          });
        }
      }

      e.preventDefault();
    });
  });
}

// App Scroll
var defaultScroll = Math.ceil(window.pageYOffset);
var setScrollUp = document.getElementById("scrollUp");

window.addEventListener("scroll", () => {
  var lengthScroll = Math.ceil(window.pageYOffset);

  if (setNavbar != null && lengthScroll > setNavbar.clientHeight - 15) {
    setNavbar.classList.add("active");
  } else {
    setNavbar.classList.remove("active");
  }

  lengthScroll > 728
    ? setScrollUp.classList.add("active")
    : setScrollUp.classList.remove("active");
  lengthScroll = defaultScroll;
});

// App Load
window.addEventListener("load", () => {
  // Data Loading
  let getLoading = document.querySelectorAll("[data-loading]");

  if (getLoading.length !== 0) {
    getLoading.forEach((tgetEl) => {
      var setLoading = tgetEl.getAttribute("data-loading") !== null;

      if (setLoading === true) {
        if (!tgetEl.classList.contains("hidden")) {
          tgetEl.classList.add("hidden");
        } else {
          tgetEl.classList.remove("hidden");
        }
      }
    });
  }
});
